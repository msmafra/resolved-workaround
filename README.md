# Resolved workaround

A watchdog service for systemd-resolved that restarts it when it stops resolving domains.  
Assumes DNSSEC is used.  
  
Deploy using deploy.sh.
