#!/usr/bin/bash

cp resolved_workaround /usr/bin/resolved_workaround
chown root.root /usr/bin/resolved_workaround
chmod 740 /usr/bin/resolved_workaround

cp logrotate.conf /etc/logrotate.d/resolved_workaround
chown root.root /etc/logrotate.d/resolved_workaround
chmod 640 /etc/logrotate.d/resolved_workaround

cp resolved-workaround.service /usr/lib/systemd/system/
chown root.root /usr/lib/systemd/system/resolved-workaround.service
chmod 640 /usr/lib/systemd/system/resolved-workaround.service

systemctl daemon-reload
systemctl start resolved-workaround
